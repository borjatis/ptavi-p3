import smil
import sys


def main(path=None):
    if path is None:
        print("Usage: python3 elements.py <file>")
    else:
        object = smil.SMIL(path)
        for element in object.elements():
            print(element.name())


if __name__ == "__main__":
    main(sys.argv[1])
