#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom

import smil


class Element:
    def __init__(self, element_name, attribute_dictionary, element_content):
        self.element_name = element_name
        self.attribute_dictionary = attribute_dictionary
        self.element_content = element_content

    def name(self):
        return self.element_name

    def attrs(self):
        return self.attribute_dictionary

    def children(self):
        pass


class SMIL:

    def __init__(self, path):
        self.path = path

    def elements(self):
        file = xml.etree.ElementTree.parse(self.path)
        elements = []
        tipo = []

        for element in file.iter():
            elements.append(element)

        for type_element in elements:
            tipo.append(Element(type_element.tag, type_element.attrib, elements))

        return tipo


if __name__ == '__main__':
    object = SMIL('karaoke.smil')
    element = object.elements()
