#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main(path):
    obj = Humor(path)
    for joke in obj.jokes():
        print(f"Calificación: {joke['score']}.")
        print(f" Respuesta: {(joke['answer'])}")
        print(f" Pregunta: {(joke['question'])}\n")


class Humor:
    def __init__(self, path):
        self.path = path

    def jokes(self):
        """Programa principal"""
        document = xml.dom.minidom.parse('self.path')
        if document.getElementsByTagName('humor'):
            path = document.getElementsByTagName('chiste')
            jokes = []
            list = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
            for joke in path:
                score = joke.getAttribute('calificacion')
                questions = joke.getElementsByTagName('pregunta')
                question = questions[0].firstChild.nodeValue.strip()
                answers = joke.getElementsByTagName('respuesta')
                answer = answers[0].firstChild.nodeValue.strip()
                jokes.append((score, question, answer))

                for i in list:
                    for j in jokes:
                        k = j[0]
                        if i == k:
                            print(f"Calificación: {i}.")
                            print(f" Pregunta: {(j[1])}")
                            print(f" Respuesta: {j[2]}\n")
                            break
        else:
            raise Exception("Root element is not humor")


if __name__ == "__main__":
    main()
